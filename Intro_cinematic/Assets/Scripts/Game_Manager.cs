﻿using UnityEngine;
using System.Collections;

public class Game_Manager : MonoBehaviour {

	// Use this for initialization
	void OnEnable ()
	{
	    StartCoroutine(Wait(85.0f));
	}
	
	// Update is called once per frame
	void Update () {

	}

    private IEnumerator Wait(float _duration)
    {
        yield return new WaitForSeconds(_duration);
        Application.CancelQuit();
    }
}
