﻿using System;
using System.ComponentModel;
using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;
using System.Collections.Generic;
using UnityEditor;

[System.Serializable]
public class CameraObj
{
    public string m_name = "";
    public Transform m_attached_to = null;
    public LayerMask m_culling_mask = -1;
    public float m_field_of_view = 60.0f;
    public bool m_orthographic = false;
    public float m_near_clipping_plane = 0.3f;
    public float m_far_clipping_plane = 500.0f;
    public Vector3 m_relative_position = new Vector3(0f,0f,0f);
    public Vector3 m_relative_orientation = new Vector3(0f, 0f, 0f);
    public RuntimeAnimatorController m_animator = null;
    public Transform m_look_at = null;

    public static CameraObj select_camera(string typeOfCamera)
    {
        foreach (var camObj in CameraManager.instance.m_camera_list)
        {
            if (camObj.m_name == typeOfCamera)
            {
                return camObj;
            }
        }
        return null;
    }
}

[System.Serializable]
public class Sequence
{
    public string m_active_camera = "";
    public float m_start_time = 0.0f;
    public float m_end_time = 0.0f;
}

public class CameraManager : MonoBehaviour {

    // instance de classe (Singleton)
    public static CameraManager instance = null;
    // Paramétrage des caméras
    public string m_active_camera = "";
    public List<CameraObj> m_camera_list = new List<CameraObj>();
    public List<Sequence> m_sequences = new List<Sequence>();
    public Dictionary<string, GameObject> CameraDictionary = new Dictionary<string, GameObject>();
    
    private float m_time = 0.0f;
    private float m_next_check = 0.0f;

    // declare l'instance du singleton et crée les caméras
    void OnEnable()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }

        foreach (var cam in m_camera_list)
        {
            create_camera(cam.m_name, cam.m_name);
        }
    }
    
    public void create_camera(string type_of_camera_, string name_)
    {
        var goGameObject = new GameObject(name_, typeof(Camera));
        goGameObject.layer = LayerMask.NameToLayer("Camera");
        var camComponent = goGameObject.GetComponent<Camera>();
        camComponent.enabled = false;

        var typeOfCam = CameraObj.select_camera(type_of_camera_);

        camComponent.cullingMask = typeOfCam.m_culling_mask;
        camComponent.fieldOfView = typeOfCam.m_field_of_view;
        camComponent.isOrthoGraphic = typeOfCam.m_orthographic;
        camComponent.nearClipPlane = typeOfCam.m_near_clipping_plane;
        camComponent.farClipPlane = typeOfCam.m_far_clipping_plane;
        
        goGameObject.transform.parent = typeOfCam.m_attached_to;
        goGameObject.transform.localPosition = new Vector3(typeOfCam.m_relative_position.x, typeOfCam.m_relative_position.y, typeOfCam.m_relative_position.z);
        goGameObject.transform.localRotation = Quaternion.Euler(typeOfCam.m_relative_orientation.x, typeOfCam.m_relative_orientation.y, typeOfCam.m_relative_orientation.z);

        CameraDictionary.Add(name_, goGameObject);

        goGameObject.AddComponent<AudioListener>();
        goGameObject.GetComponent<AudioListener>().enabled = false;

        if (typeOfCam.m_animator != null)
        {
            goGameObject.AddComponent<Animator>();
            goGameObject.GetComponent<Animator>().runtimeAnimatorController = (typeOfCam.m_animator);
        }

        if (typeOfCam.m_look_at != null)
        {
            goGameObject.AddComponent<CameraScript>();
            var camScript = goGameObject.GetComponent<CameraScript>();
            camScript.m_target = typeOfCam.m_look_at;

        }
    }

    public void set_active(string cameraName)
    {
        if (m_active_camera == "")
        {
            m_active_camera = instance.m_sequences[0].m_active_camera;
        }

        GameObject currentCam = null;
        CameraDictionary.TryGetValue(m_active_camera, out currentCam);

        GameObject camGameObject = CameraDictionary[cameraName];

        if (camGameObject != null)
        {
            camGameObject.camera.enabled = true;
            camGameObject.GetComponent<AudioListener>().enabled = true;
            CameraManager.instance.m_active_camera = cameraName;
        }
        else
        {
            CameraManager.instance.create_camera(cameraName, cameraName);
        }
        
        if (currentCam != null && currentCam != camGameObject)
        {
            currentCam.camera.enabled = false;
            currentCam.GetComponent<AudioListener>().enabled = false;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        m_time += Time.deltaTime;
        
        if (m_time >= m_next_check)
        {
            foreach (var seq in m_sequences)
            {
                if (seq.m_start_time < m_time && m_time < seq.m_end_time)
                {
                    set_active(seq.m_active_camera);
                    m_next_check = seq.m_end_time;
                    break;
                }
            }
        }

    }
}

