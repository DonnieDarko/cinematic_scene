﻿using UnityEngine;
using System.Collections;

public class character_manager : MonoBehaviour
{

    //public AnimationClip m_walking_animation = null;
    public GameObject m_door_left = null;
    public GameObject m_door_right = null;

    private Animator m_animator = null;
    private bool m_playing_walk = false;

	// Use this for initialization
	void OnEnable ()
	{
	    m_animator = gameObject.GetComponentInParent<Animator>();
	}

    // Update is called once per frame
    void Update()
    {
        walkingSound();
    }

    void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Triggered " + collision.gameObject.name);
        if (collision.gameObject.name == "TrigDoors")
        {
            collision.gameObject.collider.enabled = false;
            m_animator.SetTrigger("Trigger_door");
            m_door_left.GetComponent<Animator>().SetTrigger("Trigger_door");
            m_door_right.GetComponent<Animator>().SetTrigger("Trigger_door");
            AudioPlayer.CreatePlayAudioObject(BaseManager.instance.m_sound["grincement"], m_door_left.transform);
        }

        if (collision.gameObject.name == "PastDoors")
        {
            collision.gameObject.collider.enabled = false;
            m_animator.SetTrigger("Wait");
            m_door_left.GetComponent<Animator>().SetTrigger("Wait");
            m_door_right.GetComponent<Animator>().SetTrigger("Wait");
            AudioPlayer.CreatePlayAudioObject(BaseManager.instance.m_sound["grincement"], m_door_left.transform);
        }

        if (collision.gameObject.name == "LastStand")
        {
            collision.gameObject.collider.enabled = false;
            m_animator.SetTrigger("Last_trigger");
            AudioPlayer.CreatePlayAudioObject(BaseManager.instance.m_sound["Scream"], m_door_left.transform);
            StartCoroutine(Wait(8.0f));
        }
    }


    private void walkingSound()
    {
        if (m_animator.GetCurrentAnimatorStateInfo(0).IsName("Walk") && !m_playing_walk)
        {
            AudioPlayer.CreatePlayAudioObject(BaseManager.instance.m_sound["Walk in armor"], this.transform);
            AudioPlayer.CreatePlayAudioObject(BaseManager.instance.m_sound["Walk on mud"], this.transform);
            m_playing_walk = true;
        }
        else if (!m_animator.GetCurrentAnimatorStateInfo(0).IsName("Walk") && m_playing_walk)
        {
            Destroy(GameObject.Find("Walk in armor"));
            Destroy(GameObject.Find("Walk on mud"));
            AudioPlayer.CreatePlayAudioObject(BaseManager.instance.m_sound["charognards"], this.transform);
            m_playing_walk = false;
        }
    }

    IEnumerator Wait(float _duration)
    {
        yield return new WaitForSeconds(_duration);
        m_animator.SetTrigger("Escape");
    }


}
