﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public enum Channel_choice {Musics, Sfx_Sounds, Ambient_Sounds}

// Paramétrage unitaire d'un audioclip
[System.Serializable]
public class Sound
{
    // Données de base du son : Canal, clip et nom
    public Channel_choice m_channel;
    public AudioClip m_audioclip;
    public string m_name = "";
    // Volume du son
    [Range(0.0f, 1.0f)]
        public float m_volume = 1.0f;
    public bool m_mute = false;
    // Clé (sous forme de string) permettant de jouer en random parmi les sons qui la partagent
    public string m_random_key = "";
    // Type de son 2D/3D, en lecture seule car la modification charge le fichier de nouveau
    public bool m_2D_sound = false;
    public bool m_3D_sound = false;
    // Valeur de fading (FadeIn si >0 et FadeOut si <0)
    [Range(-1.0f, 1.0f)]
        public float m_fade_value = 0.0f;
    // Loop et pitch
    public bool m_loop = false;
    [Range(0.0f, 50.0f)]
        public float m_pitch_range = 0.0f; // Le pitch varie autour de 1 dans la limite de cette valeur (ex : 0.8 1.2 pour une valeur de 100%)

    // Methode permettant de savoir si un son est actuellement actif
    public bool isActive() {
        Channel soundChannel = BaseManager.instance.getSoundChannel(this);
        return soundChannel.m_active.Contains(m_name);
    }
}

// Paramétrage d'un canal
[System.Serializable]
public class Channel
{
    [Range(0.0f, 1.0f)]
        public float m_volume = 1.0f;
    public bool m_mute = false;
    // Listes utilisées pour afficher les sons actifs/inactifs des canaux
    public List<string> m_inactive = new List<string>();
    public List<string> m_active = new List<string>();
    // Dictionnaire du canal, pour tenir à jour l'état actuf/inactif de chaque son (par ID)
    public Dictionary<long, string> playing_ID = new Dictionary<long, string>();
    
    // liste de sons du canal
    private List<string> m_all_sounds = new List<string>();
    
    // Permet d'initialiser la liste des sons par canal
    public void setAllSoundsList(List<string> _soundsList) {
        m_all_sounds.AddRange(_soundsList);
    }
    // et de l'utiliser notamment dans le script Register
    public List<string> getAllSoundsList()
    {
        return m_all_sounds;
    }

    // Pour un son donné (d'après son nom), renvoie la liste des soundObjects
    public List<long> getPlayingSoundIDList(string _soundName)
    {
        List<long> list_ID = new List<long>();
        foreach (long key in this.playing_ID.Keys)
        {
            if (this.playing_ID[key] == _soundName)
            {
                list_ID.Add(key);
            }
        }
        return list_ID;
    }


}
 /******************************************************
 * SOUND MANAGER                                       *
 * Cette classe est utilisée pour créer un singleton   *
 * qui permet de définir                               *
 *     - Une playslist                                 *
 *     - Les canaux de lecture                         *
 *     - Les paramètres globaux (Mute, master volume)  *
 * Permet aussi de réaliser des tests                  *
 ******************************************************/
public class BaseManager : MonoBehaviour
{
    // instance de classe (Singleton)
    public static BaseManager instance = null;
    
    // Liste de paramétrage de tous les audioclips
    public List<Sound> m_playlist = new List<Sound>();
    public Dictionary<string, Sound> m_sound = new Dictionary<string, Sound>();

    // Activation des boutons de tests audio et coupure globale du son
    public bool m_playtests = false;
    public bool m_mute_all = false;
    // Master Volume
    [Range(0.0f, 1.0f)]
        public float m_master_volume = 1.0f;
    // Channels
    public Channel m_musics;
    public Channel m_sfx_Sounds;
    public Channel m_ambient_Sounds;

    // declare l'instance du singleton et met à jour les listes et dictionnaires de canaux
    void OnEnable()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }

        for (int i = 0; i < m_playlist.Count; ++i) {
            // Mise à jour des infos 2D/3D d'après la source audioclip
            m_playlist[i].m_2D_sound = !Toggle3DSoundSettings(m_playlist[i].m_audioclip);
            m_playlist[i].m_3D_sound = Toggle3DSoundSettings(m_playlist[i].m_audioclip);
            
            // Mise à jour du dictionnaire des sons
            this.m_sound.Add(m_playlist[i].m_name, m_playlist[i]);
            
            // Initialisation des listes de canaux
            if (m_playlist[i].m_channel.Equals(Channel_choice.Musics))
            {
               this.m_musics.m_inactive.Add(m_playlist[i].m_name);
            }
            if (m_playlist[i].m_channel.Equals(Channel_choice.Sfx_Sounds))
            {
                this.m_sfx_Sounds.m_inactive.Add(m_playlist[i].m_name);
            }
            if (m_playlist[i].m_channel.Equals(Channel_choice.Ambient_Sounds))
            {
                this.m_ambient_Sounds.m_inactive.Add(m_playlist[i].m_name);
            }

            // Initialise la liste globale de sons, par canaux
            this.m_musics.setAllSoundsList(this.m_musics.m_inactive);
            this.m_sfx_Sounds.setAllSoundsList(this.m_sfx_Sounds.m_inactive);
            this.m_ambient_Sounds.setAllSoundsList(this.m_ambient_Sounds.m_inactive);
        }
    }
    
    // GUI pour manipuler les sons et passer d'une scène à l'autre
    int OnGUI()
    {
        if (m_playtests == false) {
            return 0;
        }
        
        // Zone de boutons
        GUILayout.BeginArea(new Rect(20, 20, 400, 600));
        GUILayout.Space(100);
        GUILayout.Label("Load a scene and fade in a looped Audio Clip");
        if (GUILayout.Button("Menu Scene", GUILayout.Height(60)))
        {
            Application.LoadLevel
                (0);
        }
        if (GUILayout.Button("Game Scene", GUILayout.Height(60)))
        {
            Application.LoadLevel
                (1);
        }
        GUILayout.Space(50);
        GUILayout.Label("Play a single sound");
     
        createButtons(new List<Sound>(m_sound.Values));
        GUILayout.Space(50);

        GUILayout.EndArea();
        return 0;
    }

    // Création des boutons d'activation des sons
    void createButtons(List<Sound> lSound) {
        int i = 0;
        
        while (i < lSound.Count)
        {
            if (i % 2 == 0)
            {
                GUILayout.BeginHorizontal();
            }
            // Joue le son correspondant au bouton
            string s = lSound[i].m_name;
            if (GUILayout.Button("Play "+ s) && !lSound[i].isActive())
            {
                AudioPlayer.CreatePlayAudioObject(lSound[i], Camera.main.transform);
            }
            i++;
            if (i % 2 == 0)
            {
                GUILayout.EndHorizontal();
            }
        }
    }


    // Utile pour accéder à un son par son nom
    public Sound getSoundbyName(string _sound_name) {
        Sound mySound = null;
        foreach (Sound s in m_sound.Values) {
            if (s.m_name == _sound_name) {
                mySound = s;
            }
        }
        return mySound;
    }

    // Utile pour accéder directement au Register d'un soundObject (par le nom et via l'ID)
    // ne renvoie que le 1er si plusieurs sons identiques sont joués
    public Register getObjSoundRegister(string _soundName)
    {
        Sound _sound = this.getSoundbyName(_soundName);
        Channel _channel = this.getSoundChannel(_sound);
        int id = (int)_channel.getPlayingSoundIDList(_soundName)[0];

        return (Register)EditorUtility.InstanceIDToObject(id);
    }
    
    // cette fonction retourne le canal du son passé en paramètre
    public Channel getSoundChannel(Sound _sound) {
        
        Channel myChannel = null;
        if (_sound.m_channel.Equals(Channel_choice.Ambient_Sounds)){
            myChannel = m_ambient_Sounds;
        }
        if (_sound.m_channel.Equals(Channel_choice.Musics))
        {
            myChannel = m_musics;
        }
        if (_sound.m_channel.Equals(Channel_choice.Sfx_Sounds))
        {
            myChannel = m_sfx_Sounds;
        }
        return myChannel;
    }

    // Renvoie le type 2D/3D d'un AudioClip
    bool Toggle3DSoundSettings(AudioClip _audioclip) {
        string path = AssetDatabase.GetAssetPath(_audioclip);
        AudioImporter audioImporter = AssetImporter.GetAtPath(path) as AudioImporter;
        return audioImporter.threeD;
    }


}

