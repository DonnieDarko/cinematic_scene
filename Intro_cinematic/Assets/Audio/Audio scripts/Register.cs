﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Register : MonoBehaviour {
    public string m_channel_name = "";
    public string m_sound_name = "";
    public float m_fade_value = .0f;
    private Channel m_objChannel = null;    // Canal du son
    private AudioSource m_audioSource = null;   // AudioSource du gameObject
    private float m_nominalVolume = 0.0f;   // Volume de base à la création du gameobject
    private float m_currentVolume = 0.0f;   // Volume courant

    /****************************************************************
    * Ce script est attaché à tous les gameObjects porteurs de sons *
    * et il met à jour le dictionnaire des canaux (m_playing_ID)    *
    ****************************************************************/

	// Use this for initialization
	void Start () {
        // Mise à jour du canal
        getChannelName();
        m_objChannel.playing_ID.Add(this.GetInstanceID(), m_sound_name);
        updateChannelLists();
        
        // Initialisation de l'effet de fading
        m_audioSource = this.GetComponent<AudioSource>();
        m_nominalVolume = m_audioSource.volume;
        m_currentVolume = m_audioSource.volume;
        // Un son possédant un effet de FadeIn commence avec un volume nul
        if (m_fade_value > 0.0f)
        {
            m_audioSource.volume = 0.0f;
        }
	}

    void FixedUpdate() {
        // mise à jour du volume courant
        updateVolume();
        // Mise à jour du volume par le fading
        if (m_fade_value != 0.0f)
        {
            fadeSound();
        }
        else {
            m_audioSource.volume = m_currentVolume;
        }
        // Mute le son si un des contrôleurs est coupé
        m_audioSource.mute = (BaseManager.instance.m_mute_all || m_objChannel.m_mute || BaseManager.instance.getSoundbyName(m_sound_name).m_mute);
    }

    void fadeSound() {
        // Augmente/ diminue progressivement le volume selon la valeur du fading
        m_audioSource.volume += m_fade_value / 100;
        if (m_audioSource.volume == 0 && m_fade_value < 0.0f)
        {
            Destroy(gameObject);
        }
        if (m_audioSource.volume > m_currentVolume)
        {
            m_audioSource.volume = m_currentVolume;
            m_fade_value = 0.0f;
        }
    }

    void updateVolume()
    {
        // Mise à jour du volume à la valeur mini parmi toutes les valeurs de contrôle
        m_currentVolume =  Mathf.Min(BaseManager.instance.getSoundbyName(m_sound_name).m_volume,m_nominalVolume) 
                           * BaseManager.instance.m_master_volume
                           * m_objChannel.m_volume;
    }

    void getChannelName() {

        // Met à jour la variable publique du script pour le nom du canal
        if (m_objChannel.Equals(BaseManager.instance.m_ambient_Sounds))
        {
            m_channel_name = "Ambient_Sounds";
        }
        if (m_objChannel.Equals(BaseManager.instance.m_musics))
        {
            m_channel_name = "Musics";
        }
        if (m_objChannel.Equals(BaseManager.instance.m_sfx_Sounds))
        {
            m_channel_name = "Sfx_Sounds";
        }
    }

    public void setChannel(Channel _channel) {
        // accesseur
        m_objChannel = _channel;
    }

    public Channel getChannel() {
        // accesseur
        return m_objChannel;
    }

	void OnDestroy () {
        // Met à jour le dictionnaire de sons du canal lorsque le gameobject porteur du son est détruit
        m_objChannel.playing_ID.Remove(this.GetInstanceID());
        updateChannelLists();
	}

    void updateChannelLists() {

        // Parcours la liste de valeurs du dictionnaire de sons (pour le canal considéré)
        // pour mettre à jour la liste des sons actifs / inactifs
        foreach (string s in m_objChannel.playing_ID.Values)
        {
            if (!m_objChannel.m_active.Contains(s))
            {
                m_objChannel.m_active.Add(s);
            }
            if (m_objChannel.m_inactive.Contains(s))
            {
                m_objChannel.m_inactive.Remove(s);
            }
        }


        // Idem, mais à partir de la liste globale des sons pour mettre à jours
        // ceux qui n'appartiennent pas au dictionnaire (et qui sont donc inactifs)
        foreach (string s in m_objChannel.getAllSoundsList())
        {
            if (!m_objChannel.playing_ID.ContainsValue(s) && !m_objChannel.m_inactive.Contains(s))
            {
                m_objChannel.m_inactive.Add(s);
                if (m_objChannel.m_active.Contains(s))
                {
                    m_objChannel.m_active.Remove(s);
                }
            }
        }
        
    }
}
