﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class GameSoundManager : MonoBehaviour {

    public Transform localSounds = null;

	// Use this for initialization
    void Start()
    {
        Sound introMusic = BaseManager.instance.getSoundbyName("intro_music");
        Sound gameMusic = BaseManager.instance.getSoundbyName("game_music");
        
        // FadeOut de la musique d'intro lors d'un changement de scène
        if (introMusic.isActive())
        {
            Register objRegister = BaseManager.instance.getObjSoundRegister("intro_music");
            objRegister.m_fade_value = -introMusic.m_fade_value;
        }

        // Demarrage de la musique du jeu
        if (!gameMusic.isActive())
        {
            AudioPlayer.CreatePlayAudioObject(gameMusic, BaseManager.instance.transform);
        }
        // et des sons d'ambiance
        foreach (string ambientSound in BaseManager.instance.m_ambient_Sounds.m_inactive)
        {
            Sound mySound = BaseManager.instance.getSoundbyName(ambientSound);
            if (!mySound.isActive()) {
                AudioPlayer.CreatePlayAudioObject(mySound, localSounds);
            }
        }


    }

    void Update() {

    }

}
