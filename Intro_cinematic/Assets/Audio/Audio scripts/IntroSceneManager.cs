﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class IntroSceneManager : MonoBehaviour {

    // Use this for initialization
    void Start() 
    {
        Sound introMusic = BaseManager.instance.getSoundbyName("intro_music");
        Sound gameMusic = BaseManager.instance.getSoundbyName("game_music");
        
        // FadeOut de la musique du jeu lors d'un changement de scène
        if (gameMusic.isActive())
        {
            Register objRegister = BaseManager.instance.getObjSoundRegister("game_music");
            objRegister.m_fade_value = -gameMusic.m_fade_value;
        }

        if (!introMusic.isActive())
        {
            AudioPlayer.CreatePlayAudioObject(introMusic, BaseManager.instance.transform);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
