﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/*****************************************************************
* Cette classe créé les gameobjects qui jouent les sons          *
* Les paramètres des AudioSources attachés aux gameobjects       *
* sont directement issus des sons de la playlist du SoundManager *
******************************************************************/

public class AudioPlayer : MonoBehaviour {

    // La fonction ci-dessous crée un gameobject doté d'un composant Audio Source qui joue le son défini
    public static GameObject CreatePlayAudioObject(Sound mySound, Transform attachTo)
    {
        if (mySound.m_random_key != "") {
            mySound = playRandomSound(mySound);
        }
        // Défintion du canal de son pour la suite
        Channel myChannel = BaseManager.instance.getSoundChannel(mySound);
        
        // instantiation d'un nouveau gameobject
        GameObject apObject = new GameObject(mySound.m_name);
        apObject.layer = LayerMask.NameToLayer("Sounds");
        apObject.tag = "Sounds";
        
        // Ajout d'un script de registre (Voir Register.cs)
        apObject.AddComponent("Register");
        Register apScript = apObject.GetComponent<Register>();
        apScript.setChannel(myChannel);
        apScript.m_sound_name = mySound.m_name;
        apScript.m_fade_value = mySound.m_fade_value;
        
        // qu'on attache au gameobject souhaité (utile pour le son 3D)
        apObject.transform.parent = attachTo;

        // Ajout d'un composant AudioSource
        apObject.AddComponent<AudioSource>();
        AudioSource apAudio = apObject.GetComponent<AudioSource>();
        
        // Initialisation des paramètres Audio Source
        bool isMute = mySound.m_mute || myChannel.m_mute || BaseManager.instance.m_mute_all;
        apAudio.mute = isMute;
        apAudio.playOnAwake = false;
        apAudio.rolloffMode = AudioRolloffMode.Linear;

        apAudio.loop = mySound.m_loop;
        apAudio.clip = mySound.m_audioclip;

        float playingVolume = Mathf.Min(mySound.m_volume, myChannel.m_volume, BaseManager.instance.m_master_volume);
        apAudio.volume = playingVolume;  // Le volume est au mini parmi le master volume, celui du canal et celui du son unitaire

        // Mise en place d'un pitch de base
        UnityEngine.Random.seed = (int)DateTime.Now.Ticks;
        apAudio.pitch = 1.0f + UnityEngine.Random.Range(-mySound.m_pitch_range, mySound.m_pitch_range) / 100.0f;

        // Activation de l'effet sonore
        apAudio.Play();
        
        // Destruction du gameobject au terme de l'effet sonore
        if (!mySound.m_loop)
        {
            Destroy(apObject, mySound.m_audioclip.length);
        }

        return apObject;
    }

    
    // Cette fonction renvoie un son aléatoire parmi ceux qui ont été définis avec la même clé
    public static Sound playRandomSound(Sound mySound) {
        string randomKey = mySound.m_random_key;
        List<Sound> randomSounds = new List<Sound>();

        // Recherche des sons possédant une clé commune
        foreach (Sound s in BaseManager.instance.m_sound.Values) {
            if (s.m_random_key != "" && s.m_random_key == randomKey) {
                randomSounds.Add(s);
            }
        }

        int rnd = UnityEngine.Random.Range(0, randomSounds.Count);

        return randomSounds[rnd];
    }

}
